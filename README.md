# Buynomics
#### dan.dedkov@gmail.com

---

Run `npm install` and grab a coffee, while it installs Nx, Cypress and other small things

## Development server

Run `docker run -d -p 5555:5432 -e POSTGRES_DB=buynomics -e POSTGRES_USER=buynomics postgres:11.1-alpine`

Run `nx serve backend`

Run `nx serve frontend` -> Navigate to http://localhost:4200/

## Running unit tests

Run `nx test backend`

Run `nx test frontend`

Run `nx test intermediary` - testing submodule with data stuff

Run `nx test utils` - testing another submodule

Kill Dev Server and run `nx e2e frontend-e2e` for automated testing with Cypress

Run `nx dep-graph` to see a diagram of the dependencies.

