export function formatDate(d: Date): string {
  let options = { day: 'numeric', month: 'short', year: 'numeric', hour: 'numeric', minute: 'numeric', timeZone: 'UTC', timeZoneName: 'short', hour12: false};
  const str = new Intl.DateTimeFormat('en-AU', options as any).format(d);
  return str
}

export function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

export function removeFromArray(array, index): any[] {
  const upd = [...array]
  upd.splice(index, 1);
  return [...upd]
}

export function remplaceInArray(array, index, val): any[] {
  const upd = [...array]
  upd.splice(index, 1, val);
  return [...upd]
}

export function idValidDecimal(num): boolean {
  const dec = /^\d+\.\d{0,6}$/;
  const int = /^\d+$/;
  return dec.test(num) || int.test(num)
}


