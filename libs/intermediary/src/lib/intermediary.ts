import { idValidDecimal } from '@buynomics/utils'
import * as _ from 'lodash'

export enum IntermediaryType {Range = 'Range', Dropdown = 'Dropdown'}

export type IntermediaryId = number

export class Intermediary {
  id: IntermediaryId
  created_on: Date
  name: string
  order: number
  type: IntermediaryType
  range_options?: RangeOptions
  dropdown_options?: DropdownOptions[]
}

export interface RangeOptions {
  from: number
  to: number
  step: number
}

export interface DropdownOptions {
  option: string
  value: number
}

export function newIntermediary(x: Intermediary) {
  return {
    id: x.id ? parseInt(x.id.toString()) : null,
    created_on: x.created_on ? new Date(x.created_on) : new Date(),
    name: x.name || '',
    order: x?.order ? parseInt(x?.order.toString()) : null,
    type: x.type,
    range_options: !_.isEmpty(x?.range_options) ? {
      from: parseFloat(x?.range_options?.from?.toString()),
      to: parseFloat(x?.range_options?.to?.toString()),
      step: parseFloat(x?.range_options?.step?.toString())
    } : null,
    dropdown_options: (x?.dropdown_options || []).map(opt => ({
      option: opt?.option || '',
      value: parseFloat(opt?.value?.toString())
    })),
  }
}

export function setIntermediaryProp(x: Intermediary, prop: string, val: any): Intermediary {
  const updated = {...x, [prop]: val}
  return newIntermediary(updated)
}

export function checkTransition(a: Intermediary, b: Intermediary): string {
  let err = checkIntermediary(b)

  if(err) return err

  if (a?.type === IntermediaryType.Range && b?.type === IntermediaryType.Range) {
    const check1 = (b?.range_options?.from > a?.range_options?.from)
    const check2 = (a?.range_options?.from - b?.range_options?.from) / b?.range_options?.step
    if (check1)
      return 'err: from can not become bigger'
    if (!Number.isInteger(check2) || check2 <= 0)
      return 'err: from can not be reached'
  }

  return ''
}

export function checkIntermediary(x: Intermediary): string {
  if (x?.type === IntermediaryType.Range) {
    if (x?.range_options?.from != null && x?.range_options?.to != null && x?.range_options?.step != null) {
      if (!idValidDecimal(x?.range_options?.from) || !idValidDecimal(x?.range_options?.to) || !idValidDecimal(x?.range_options?.step)) return 'err: non-numeric value'
      if (x?.range_options?.from >= x?.range_options?.to) return 'err: from can not be bigger than to'
      if (x?.range_options?.step <= 0) return 'err: step can not be negative'
    }
  } else if (x?.type === IntermediaryType.Dropdown) {
    if (x?.dropdown_options?.some(opt => !idValidDecimal(opt?.value))) return 'err: non-numeric value'
  }

  if (!x?.name || !x?.order || !x?.type) {
    return 'err: missing data'
  }

  return ''
}
