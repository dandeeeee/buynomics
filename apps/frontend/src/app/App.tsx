import styles from './app.module.scss';

import React, { useReducer } from 'react';
import { Route, Link, useLocation } from 'react-router-dom';
import { IntermediariesList } from './intermediaries/IntermediariesList';
import { IntermediaryDetails } from './intermediaries/IntermediaryDetails';

export function App() {
  let location = useLocation()

  return (
    <div className={styles.app}>

      <div role="navigation">
        <ul data-cy="main-nav">
          <li><Link to="/intermediaries">Intermediaries</Link></li>
          {/*<li><Link to="/products">Products</Link></li>*/}
        </ul>
      </div>

      <Route
        path="/intermediaries"
        exact
        render={() => <IntermediariesList />}
      />

      <Route
        path="/intermediaries/details"
        exact
        render={() => {
          return <IntermediaryDetails intermediary={location.state} />
        }}
      />

      {/*<Route*/}
      {/*  path="/page-2"*/}
      {/*  exact*/}
      {/*  render={() => <ProductsList />}*/}
      {/*/>*/}

    </div>
  );
}

export default App;
