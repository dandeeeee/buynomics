import { Intermediary } from '@buynomics/intermediary';
import { formatDate } from '@buynomics/utils';
import React from 'react'
import { Link } from 'react-router-dom';
import { useAsyncState } from '../state/useAsyncState';
import { IntermediariesAPI } from './IntermediariesAPI';

export function IntermediariesList () {

  const loader = React.useCallback(() => IntermediariesAPI.fetchAll(), [])
  const [{payload: intermediaries, isLoading, isError}, dispatch, reFetch] = useAsyncState<Intermediary[]>('intermediaries', loader)
  const handleDelete = React.useCallback((int) => reFetch(() => IntermediariesAPI.delete(int.id)), [])

  if (isLoading) {
    return <p data-cy="loading-intermediaries">'Loading...'</p>
  } else if (isError) {
    return 'No Backend or error'
  } else {
    const items = intermediaries?.map((int: Intermediary) => (
      <tr key={`intermediary-${int.id}`}>
        <td>{formatDate(int.created_on)}</td>
        <td>{int.order}</td>
        <td><Link to={{pathname: '/intermediaries/details', state: int }}>Edit</Link>, <button onClick={() => handleDelete(int)}>Delete</button></td>
      </tr>
    ))

    return (<div>
      {items && (
        <table border={1}>
          <tbody>
            {items}
          </tbody>
        </table>
      )}
      <button><Link to={{pathname: '/intermediaries/details', state: {} }}>Add</Link></button>
    </div>)
  }

}
