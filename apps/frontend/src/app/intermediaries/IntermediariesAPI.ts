import { Intermediary, IntermediaryId, newIntermediary } from '@buynomics/intermediary';

export class IntermediariesAPI {
  static root = 'http://localhost:3333/api/intermediaries'

  static async fetchAll(): Promise<Intermediary[]> {
    return fetch(this.root)
      .then(res => res.json())
      .then((intermediaries: Intermediary[]) => (intermediaries || []).map(int => newIntermediary(int)))
  }

  static async create(intermediary: Intermediary): Promise<Intermediary> {
    const body = {body: JSON.stringify(intermediary)}
    const headers = {'Content-Type': 'application/json'}
    const data = await fetch(`${this.root}`, {method: 'POST', headers, ...body})
      .then(res => res.json())
    return newIntermediary(data)
  }

  static async update(intermediary: Intermediary): Promise<Intermediary> {
    const body = {body: JSON.stringify(intermediary)}
    const headers = {'Content-Type': 'application/json'}
    const data = await fetch(`${this.root}/${intermediary.id}`, {method: 'PUT', headers,  ...body})
      .then(res => res.json())
    return newIntermediary(data)
  }

  static delete(id: IntermediaryId): Promise<any> {
    return fetch(`${this.root}/${id}`, {method: 'DELETE'})
  }

}
