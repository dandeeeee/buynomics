import { render } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'

import { IntermediariesList } from '../IntermediariesList'

jest.mock('react-redux', () => ({
  useSelector: () => jest.fn(),
  useDispatch: () => jest.fn(),
}))

describe('IntermediariesList tests', () => {

  it('should render successfully', () => {
    const { baseElement } = render(
      <MemoryRouter>
        <IntermediariesList />
      </MemoryRouter>
    )

    expect(baseElement).toBeTruthy()
  })
})
