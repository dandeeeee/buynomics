import {
  checkIntermediary,
  checkTransition,
  Intermediary,
  IntermediaryType, newIntermediary,
  setIntermediaryProp
} from '@buynomics/intermediary';
import { removeFromArray, remplaceInArray } from '@buynomics/utils';
import React from 'react'
import { Link } from 'react-router-dom';
import { IntermediariesAPI } from './IntermediariesAPI';

export function IntermediaryDetails(props: {intermediary: Intermediary}) {
  const {intermediary} = props
  const [curr, setCurr] = React.useState(intermediary)
  const [err, setErr] = React.useState('')
  const [suppressErrrors, setSuppressErrrors] = React.useState(true)

  const commit = React.useCallback(async () => {
    const err = checkIntermediary(curr)
    setErr(err)
    if (!err) {
      const updated = await (curr?.id
          ? IntermediariesAPI.update(curr)
          : IntermediariesAPI.create(curr)
      ) as Intermediary
      setCurr(updated)
    }
    setSuppressErrrors(false)
  }, [curr])

  const tryUpdate = React.useCallback((prop: string, val: any) => {
    const updated = setIntermediaryProp(curr, prop, val)
    const err = checkTransition(intermediary, updated)
    if (err && !suppressErrrors) {
      setErr(err)
    }
    setCurr({...updated})
  }, [curr])

  const updateProp = React.useCallback((prop) => (e: Event) => {
    tryUpdate(prop, e.target.value)
  }, [curr])

  const setRangeOpt = React.useCallback((opt) => {
    tryUpdate('range_options', opt)
  }, [curr])

  const setOpt = React.useCallback((index, opt) => {
    tryUpdate('dropdown_options', remplaceInArray(curr?.dropdown_options || [], index, opt))
  }, [curr])

  const removeOpt = React.useCallback((index) => {
    tryUpdate('dropdown_options', removeFromArray(curr?.dropdown_options || [], index))
  }, [curr])

  const addOpt = React.useCallback(() => {
    tryUpdate('dropdown_options', [...(curr?.dropdown_options || []), {option: '', value: 0}])
  }, [curr])

  return (
    <div>
      {err && <h1>{err}</h1>}

      <input type="text" defaultValue={curr?.name} onChange={updateProp('name')} />
      <input type="number" defaultValue={curr?.order} onChange={updateProp('order')} />

      <select disabled={curr?.id} defaultValue={curr?.type} onChange={updateProp('type')}>
        <option></option>
        <option>{IntermediaryType.Range}</option>
        <option>{IntermediaryType.Dropdown}</option>
      </select>

      {curr?.type === IntermediaryType.Range && (<div>
        <input type="number" defaultValue={curr?.range_options?.from} onChange={(e) => setRangeOpt({from: (e.target.value), to: curr?.range_options?.to, step: curr?.range_options?.step})} />
        <input type="number" defaultValue={curr?.range_options?.to} onChange={(e) => setRangeOpt({to: (e.target.value), from: curr?.range_options?.from, step: curr?.range_options?.step})} />
        <input type="number" defaultValue={curr?.range_options?.step} onChange={(e) => setRangeOpt({step: (e.target.value), from: curr?.range_options?.from, to: curr?.range_options?.to})} />
      </div>)}

      {curr?.type === IntermediaryType.Dropdown && (
        <div>
          <p>Options</p>
          {(curr?.dropdown_options?.map((opt, i) => (
            <div>
              <input type="text" defaultValue={opt?.option} onChange={(e) => setOpt(i, {option: e.target.value, value: opt?.value})} />
              <input type="number" defaultValue={opt?.value} onChange={(e) => setOpt(i, {value: (e.target.value), option: opt?.option})} />
              <button onClick={() => removeOpt(i)}>Delete</button>
            </div>))
          )}
          <button onClick={addOpt}>Add</button>
        </div>
      )}

      <br />
      <br />

      <button title="Save" onClick={commit}>Save</button>
      <button><Link to="/intermediaries">Cancel</Link></button>
    </div>
  )
}
