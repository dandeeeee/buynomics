import { combineReducers, applyMiddleware, createStore } from "redux"
import { createLogger } from 'redux-logger'
import { getAsyncDataReducer } from "./asyncReducer"

export type StateProperty = string

const allAsyncReducers = (properties) => properties.reduce(
  (acc: any, stateProperty) => {
    acc[stateProperty] = getAsyncDataReducer(stateProperty);
    return acc;
  },
  {}
);

const logger = createLogger({})

export const store = (properties: string[]) => createStore(
  combineReducers(allAsyncReducers(properties)),
  applyMiddleware(logger)
);
