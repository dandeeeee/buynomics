import { StateProperty } from "./bootstrap";
import { IAction } from "./types";

export const LOADING = (stateProperty: StateProperty): string =>
  `${stateProperty}/loading`;
export const SETDATA = (stateProperty: StateProperty): string =>
  `${stateProperty}/setdata`;
export const LOAD_ERROR = (stateProperty: StateProperty): string =>
  `${stateProperty}/isError`;

export const dataLoadingAction = (stateProperty: StateProperty) => ({
  payload: {},
  type: LOADING(stateProperty)
});
export const dataUpdateAction = (
  stateProperty: StateProperty,
  payload: any
) => ({
  payload,
  type: SETDATA(stateProperty)
});
export const dataLoadingErrorAction = (
  stateProperty: StateProperty,
  error: any
) => ({
  payload: error,
  type: LOAD_ERROR(stateProperty)
});

interface IReducer {
  isLoading: boolean;
  isError: boolean;
  loadErrorDetails?: any;
  payload: any;
}
export const getAsyncDataReducer = (stateProperty: string) => {
  const reducer = (
    state: IReducer = {
      isLoading: false,
      isError: false,
      loadErrorDetails: null,
      payload: null
    },
    action: IAction
  ) => {
    switch (action.type) {
      case LOADING(stateProperty):
        return {
          ...state,
          isLoading: true,
          loadErrorDetails: null
        };
      case SETDATA(stateProperty):
        return {
          ...state,
          isLoading: false,
          loadErrorDetails: null,
          payload: action.payload
        };
      case LOAD_ERROR(stateProperty):
        return {
          ...state,
          isLoading: false,
          isError: true,
          loadErrorDetails: action.payload
        };
      default:
        return state;
    }
  };
  return reducer;
};
