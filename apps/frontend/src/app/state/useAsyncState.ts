import { useEffect, useRef, useCallback, MutableRefObject } from "react";
import { useDispatch, useSelector } from "react-redux";
import { StateProperty } from './bootstrap';
import { DispatchFunction } from "./types";
import * as _ from 'lodash'
import {
  dataUpdateAction,
  dataLoadingAction,
  dataLoadingErrorAction
} from "./asyncReducer";

interface IAsyncStateHook {
  stateProperty: StateProperty;
  loader?: (...args) => Promise<any>;
  dispatch: DispatchFunction;
  stateValue: any;
}

export const useAsyncStateHook = ({
  stateProperty,
  loader,
  dispatch,
  stateValue
}: IAsyncStateHook) => {
  const mounted: MutableRefObject<boolean> = useRef(false);

  const trigger = useCallback((op) => {
    const load = async () => {
      try {
        let result = await op();
        result = await loader(result);
        mounted.current &&
        dispatch(dataUpdateAction(stateProperty, result));
      } catch (e) {
        mounted.current && dispatch(dataLoadingErrorAction(stateProperty, e));
      }
    }

    if (!loader) {
      return;
    }
    mounted.current = true;
    dispatch(dataLoadingAction(stateProperty));
    load();
  }, [loader, stateProperty])

  useEffect(() => {
    trigger(_.noop)
    return () => {
      mounted.current = false;
    };
  }, [loader, stateProperty] as any);

  return [stateValue, dispatch, trigger];
};

export const useAsyncState = (
  stateProperty: string,
  loader?: () => any
) => {
  const dispatch = useDispatch();
  const stateValue = useSelector((state: any) => state[stateProperty]);
  if (!stateValue) {
    throw new Error(`Property ${stateProperty} not present in network state.
    Did you forget to add the reducer in combineReducers()?`);
  }
  return useAsyncStateHook({ stateProperty, loader, dispatch, stateValue } as any);
};
