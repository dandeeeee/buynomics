// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Cypress {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface Chainable<Subject> {
    goToIntermediaries(): Cypress.Chainable<any>;
  }
}

Cypress.Commands.add('goToIntermediaries', () => {
  console.log('Custom command example: goToIntermediaries')
  return cy.visit(`/intermediaries`)
});
