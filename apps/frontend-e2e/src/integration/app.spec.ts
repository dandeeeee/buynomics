import { getNav } from '../support/app.po';

describe('frontend', () => {
  beforeEach(() => cy.visit('/'));

  it('should display navigation', () => {
    getNav().contains('Intermediaries')

    cy
      .goToIntermediaries()
      .then(() => {
        cy.get('[data-cy=loading-intermediaries]').contains('Loading...')
      })
  });
});
