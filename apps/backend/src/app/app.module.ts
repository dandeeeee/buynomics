import { Module } from '@nestjs/common'
import { APP_INTERCEPTOR } from '@nestjs/core'
import { environment } from '../environments/environment'

import { LoggingInterceptor } from '@algoan/nestjs-logging-interceptor'
import { TypeOrmModule } from '@nestjs/typeorm';
import { IntermediariesModule } from './intermediary/IntermediariesModule';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: environment.dbHost,
      port: environment.dbPort,
      username: environment.dbUsername,
      password: environment.dbPass,
      database: environment.dbName,
      autoLoadEntities: true,
      synchronize: true,
    }),
    IntermediariesModule
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
  ],
})
export class AppModule {}
