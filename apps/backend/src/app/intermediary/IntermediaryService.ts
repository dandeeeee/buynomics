import { checkIntermediary, Intermediary, IntermediaryId, newIntermediary } from '@buynomics/intermediary';
import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { sleep } from '@buynomics/utils'

@Injectable()
export class IntermediaryService {

  constructor(
    @InjectRepository(Intermediary)
    private readonly repository: Repository<Intermediary>,
  ) {}

  async getBy(): Promise<Intermediary[]> {
    // await sleep(1000) // emulate latency
    return this.repository
      .createQueryBuilder("x")
      .orderBy({'x.order': 'ASC'})
      .getMany()
  }

  async create(intermediary: Intermediary): Promise<Intermediary> {
    const int = newIntermediary(intermediary)
    const err = checkIntermediary(int)
    if (err) throw new Error(err)

    return this.repository.save(int)
  }

  async updateById(id: IntermediaryId, intermediary: Intermediary): Promise<Intermediary> {
    const int = newIntermediary(intermediary)
    const err = checkIntermediary(int)
    if (err) throw new Error(err)

    await this.repository.update(intermediary.id, intermediary)

    return this.repository.findOne(intermediary.id)
  }

  async deleteById(id: IntermediaryId): Promise<any> {
    return this.repository.delete(id)
  }

}
