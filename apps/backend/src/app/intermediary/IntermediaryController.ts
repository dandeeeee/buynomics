import { Intermediary, IntermediaryId } from '@buynomics/intermediary';
import { Controller, Get, Put, Post, Delete, Param, Body, Res, HttpStatus } from '@nestjs/common'
import { Response } from 'express'

import { IntermediaryService } from './IntermediaryService'

@Controller('intermediaries')
export class IntermediaryController {
  constructor(private readonly intermediarySvc: IntermediaryService) {}

  @Get()
  async getBy(@Res() res: Response) {
    try {
      const data = await this.intermediarySvc.getBy()
      res.status(HttpStatus.OK).json(data).send()
    } catch (ex) {
      res.status(HttpStatus.BAD_REQUEST).send(JSON.stringify(ex))
    }
  }

  @Post()
  async create(@Res() res: Response, @Body() dto: Intermediary) {
    try {
      const data = await this.intermediarySvc.create(dto)
      res.status(HttpStatus.CREATED).json(data).send()
    } catch (ex) {
      console.log(JSON.stringify(ex))
      res.status(HttpStatus.BAD_REQUEST).send(JSON.stringify(ex))
    }
  }

  @Put(':id')
  async updateById(@Res() res: Response, @Param('id') id: IntermediaryId, @Body() dto: Intermediary) {
    try {
      const data = await this.intermediarySvc.updateById(id, dto)
      res.status(HttpStatus.OK).json(data).send()
    } catch (ex) {
      res.status(HttpStatus.BAD_REQUEST).send(JSON.stringify(ex))
    }
  }

  @Delete(':id')
  async deleteById(@Res() res: Response, @Param('id') id: string) {
    try {
      await this.intermediarySvc.deleteById(parseInt(id))
      res.status(HttpStatus.OK).send()
    } catch (ex) {
      res.status(HttpStatus.BAD_REQUEST).send(JSON.stringify(ex))
    }
  }

}
