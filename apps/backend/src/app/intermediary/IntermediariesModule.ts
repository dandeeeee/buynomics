import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { IntermediaryController } from './IntermediaryController';
import { IntermediaryEntity } from './IntermediaryEntity';
import { IntermediaryService } from './IntermediaryService';

@Module({
  imports: [TypeOrmModule.forFeature([IntermediaryEntity])],
  providers: [IntermediaryService],
  controllers: [IntermediaryController],
})
export class IntermediariesModule {}
