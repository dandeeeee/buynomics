import { Intermediary } from '@buynomics/intermediary';
import { EntitySchema } from 'typeorm';

export const IntermediaryEntity = new EntitySchema<Intermediary>({
  name: 'Intermediary',
  target: Intermediary,
  columns: {
    id: {
      type: Number,
      primary: true,
      generated: true,
    },
    created_on: { type: 'timestamptz' },
    name: { type: String },
    order: { type: Number },
    type: { type: String },
    range_options: { type: 'simple-json', nullable: true },
    dropdown_options: { type: 'simple-json', nullable: true },
  },
})
