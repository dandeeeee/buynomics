import { Intermediary } from '@buynomics/intermediary';
import { Test, TestingModule } from '@nestjs/testing'
import { getRepositoryToken } from '@nestjs/typeorm';
import { intermediariesStubs } from './fixtures';
import { IntermediaryService } from '../IntermediaryService'



describe('IntermediaryService Tests', () => {
  let moduleRef: TestingModule
  let svc: IntermediaryService

  beforeAll(async () => {
    moduleRef = await Test.createTestingModule({
      providers: [
        IntermediaryService,
        {
          provide: getRepositoryToken(Intermediary),
          useClass: Intermediary,
        },
      ],
    }).compile()

    svc = moduleRef.get<IntermediaryService>(IntermediaryService)
    jest.spyOn(svc, 'getBy').mockImplementation(async () => intermediariesStubs)
  })

  it('should return intermediaries', async () => {
    expect(await svc.getBy()).toHaveLength(intermediariesStubs.length)
  })

})

