import { Intermediary, IntermediaryType } from '@buynomics/intermediary';

export const intermediariesStubs: Intermediary[] = [
  {
    id: 1,
    created_on: new Date('05 May 2021 13:25 CEST'),
    name: 'US Foods',
    order: 1,
    type: IntermediaryType.Range,
    range_options: {from: 1, to: 2, step: 0.1},
  }
]
