import { Intermediary } from '@buynomics/intermediary';
import { INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing'
import { getRepositoryToken } from '@nestjs/typeorm';
import * as request from 'supertest'
import { intermediariesStubs } from './fixtures';

import { IntermediaryController } from '../IntermediaryController'
import { IntermediaryService } from '../IntermediaryService'

describe('IntermediaryController Tests', () => {
  let app: INestApplication
  let moduleRef: TestingModule
  let ctrl: IntermediaryController
  let svc: IntermediaryService

  beforeAll(async () => {
    moduleRef = await Test.createTestingModule({
      controllers: [IntermediaryController],
      providers: [
        IntermediaryService,
        {
          provide: getRepositoryToken(Intermediary),
          useClass: Intermediary,
        },
      ],
    }).compile()

    svc = moduleRef.get<IntermediaryService>(IntermediaryService)
    ctrl = moduleRef.get<IntermediaryController>(IntermediaryController)

    jest.spyOn(svc, 'getBy').mockImplementation(async () => intermediariesStubs)

    app = moduleRef.createNestApplication();
    await app.init();
  })

  it(`GET /intermediaries`, () => {
    return request(app.getHttpServer())
      .get('/intermediaries')
      .expect(200)
      .expect((res) => JSON.stringify(res) === JSON.stringify(intermediariesStubs))
  })

})

