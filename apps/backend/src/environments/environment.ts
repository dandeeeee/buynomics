export const environment = {
  production: false,
  logLevels: ['log', 'error', 'warn', 'debug', 'verbose'],

  dbHost: 'localhost',
  dbPort: 5555,
  dbUsername: 'buynomics',
  dbPass: '',
  dbName: 'buynomics',
};
