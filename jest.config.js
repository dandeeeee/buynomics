module.exports = {
  projects: [
    '<rootDir>/apps/backend',
    '<rootDir>/apps/frontend',
    '<rootDir>/libs/intermediary',
    '<rootDir>/libs/utils',
  ],
};
